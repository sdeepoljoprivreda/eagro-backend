package com.eagro.eagro.config;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.sql.Timestamp;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.eagro.eagro.entity.Refreshtoken;
import com.eagro.eagro.entity.UserLoginRequestModel;
import com.eagro.eagro.entity.Users;
import com.eagro.eagro.repository.RefreshtokenRepository;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class AuthenticationFilter extends UsernamePasswordAuthenticationFilter {
	private final AuthenticationManager authenticationManager;
	
	@Autowired
	RefreshtokenRepository repository;

	public AuthenticationFilter(AuthenticationManager authenticationManager, RefreshtokenRepository repository) {
		this.authenticationManager = authenticationManager;
		this.repository=repository;
	}

	@Override
	public Authentication attemptAuthentication(HttpServletRequest req, HttpServletResponse res)
			throws AuthenticationException {
		try {
			UserLoginRequestModel creds = new ObjectMapper().readValue(req.getInputStream(),
					UserLoginRequestModel.class);

			return authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            creds.getUsername(),
                            creds.getPassword(),
                            new ArrayList<>())
            );
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	@Override
	protected void successfulAuthentication(HttpServletRequest req,HttpServletResponse res,FilterChain chain,
											Authentication auth) throws IOException,ServletException{
		String username=((Users) auth.getPrincipal()).getUsername();
		
		
		
		Refreshtoken rtoken=new Refreshtoken();
		
		String token=Jwts.builder()
				.setSubject(username)
				.setExpiration(new Date(System.currentTimeMillis() + SecurityConstants.EXPIRATION_TIME))
                .signWith(SignatureAlgorithm.HS512, SecurityConstants.TOKEN_SECRET )
                .compact();
		String refreshtoken=Jwts.builder()
				.setSubject(username)
				.setExpiration(new Date(System.currentTimeMillis() + SecurityConstants.REFRESHEXPIRATION_TIME))
                .signWith(SignatureAlgorithm.HS512, SecurityConstants.REFRESH_SECRET )
                .compact();
		
		Timestamp date=new Timestamp(System.currentTimeMillis() + SecurityConstants.REFRESHEXPIRATION_TIME);
		
		rtoken.setExpirationdate(date);
		rtoken.setUsername(username);
		rtoken.setValue(refreshtoken);
		
		
		repository.save(rtoken);
		
		Cookie cookie=new Cookie("refreshtoken",refreshtoken);
		cookie.setMaxAge(24*24*60*60);
		cookie.setHttpOnly(true);
		
		res.addCookie(cookie);
		res.addHeader(SecurityConstants.HEADER_STRING, SecurityConstants.TOKEN_PREFIX + token);
		res.addHeader("username", username);
	}

}
