package com.eagro.eagro.config;

public class SecurityConstants {
	public static final long EXPIRATION_TIME = 864000000; // 10 days
    public static final long PASSWORD_RESET_EXPIRATION_TIME = 3600000; // 1 hour
    public static final long REFRESHEXPIRATION_TIME = 2147483647;//24days
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String REFRESHHEADER_STRING = "refreshtoken";
    public static final String SIGN_UP_URL = "/users";
    public static final String TOKEN_SECRET = "j:_7m_wQs&2NmU4WW5Fv";
    public static final String REFRESH_SECRET = "E-z7P_R8:arVrZwkuLsNy~]e\"";
    public static final String VERIFICATION_EMAIL_URL = "/users/email-verification";
    public static final String PASSWORD_RESET_REQUEST_URL = "/users/password-reset-request";
    public static final String PASSWORD_RESET_URL = "/users/password-reset";
    
    
}
