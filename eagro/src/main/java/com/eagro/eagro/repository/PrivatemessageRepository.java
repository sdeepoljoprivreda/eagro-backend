package com.eagro.eagro.repository;

import org.springframework.data.repository.CrudRepository;

import com.eagro.eagro.entity.Privatemessage;

public interface PrivatemessageRepository extends CrudRepository<Privatemessage,Integer>{

}
