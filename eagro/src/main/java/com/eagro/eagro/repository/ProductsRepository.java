package com.eagro.eagro.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.eagro.eagro.entity.Products;

public interface ProductsRepository extends CrudRepository<Products,Integer>{

	Products findByProductnameAndProducttype(String productname,String producttype);

	List<Products> findByProductname(String s);
}