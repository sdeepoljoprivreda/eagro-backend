package com.eagro.eagro.repository;

import java.util.List;
import org.springframework.data.repository.CrudRepository;

import com.eagro.eagro.entity.Group;

public interface GroupRepository extends CrudRepository<Group,Integer>{
	List<Group> findAllGroupsByUsersUsername(String username);
	List<Group> findAllGroupsByAssociationsAssociationid (int associationid);

	List<Group> findByName(String name);

	
}
