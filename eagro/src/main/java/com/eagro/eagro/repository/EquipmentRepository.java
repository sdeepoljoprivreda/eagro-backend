package com.eagro.eagro.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.eagro.eagro.entity.Equipment;
import com.eagro.eagro.entity.Group;

public interface EquipmentRepository extends CrudRepository<Equipment,Integer>{
	List<Equipment> findAllEquipmentByUsersUsername(String username);

	List<Equipment> findByName(String equipment);
}
