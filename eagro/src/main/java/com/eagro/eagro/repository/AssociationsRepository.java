package com.eagro.eagro.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.eagro.eagro.entity.Associations;

public interface AssociationsRepository extends CrudRepository<Associations,Integer>{
		List<Associations> findAllAssociationsByGroupsGroupid (int groupid);


	    List<Associations> findAllAssociationsByUsersUsername(String username);
		
		
		List<Associations> findByName(String s);
}