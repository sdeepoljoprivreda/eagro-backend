package com.eagro.eagro.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.eagro.eagro.entity.Refreshtoken;

@Repository
public interface RefreshtokenRepository extends CrudRepository<Refreshtoken,String>{

}
