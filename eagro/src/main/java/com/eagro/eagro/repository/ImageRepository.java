package com.eagro.eagro.repository;

import org.springframework.data.repository.CrudRepository;

import com.eagro.eagro.entity.Images;


public interface ImageRepository extends CrudRepository<Images,Integer>{
	
	

}
