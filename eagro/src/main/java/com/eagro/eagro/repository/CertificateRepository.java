package com.eagro.eagro.repository;

import java.util.List;
import org.springframework.data.repository.CrudRepository;

import com.eagro.eagro.entity.Certificate;

public interface CertificateRepository extends CrudRepository<Certificate,Integer> {
	
	List<Certificate> findByName(String name);
	
}
