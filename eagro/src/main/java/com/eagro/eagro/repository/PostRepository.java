package com.eagro.eagro.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.eagro.eagro.entity.Post;

public interface PostRepository extends CrudRepository<Post, Integer> {
	List<Post> findAllPostsByGroupsGroupid(int groupid);
	

	List<Post> findAllPostsByAssociationsAssociationid(int associationid);
	
	List<Post> findByParentid(int parentid);

	
}

