package com.eagro.eagro.repository;

import org.springframework.data.repository.CrudRepository;

import com.eagro.eagro.entity.Conversation;

public interface ConversationRepository extends CrudRepository<Conversation,Integer>{

}
