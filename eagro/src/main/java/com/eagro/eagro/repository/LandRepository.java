package com.eagro.eagro.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.eagro.eagro.entity.Land;
import com.eagro.eagro.entity.Users;

public interface LandRepository extends CrudRepository<Land,Integer>{

	 List<Land> findAllLandByUsersUsername(String username);
}
