package com.eagro.eagro.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;


import com.eagro.eagro.entity.Users;


public interface UserRepository extends CrudRepository<Users, String> {

    /*
     * Get user list by user name. Please note the format should be
     * findBy<column_name>.
     */
	Optional<Users> findByUsername(String username);
    
    Users findByEmail(String email);

    /*
     * Get user list by user name and password. Please note the format should be
     * findBy<column_name_1>And<column_name_2>.
     */
    List<Users> findByUsernameAndPassword(String username, String password);

    @Transactional
    void deleteByUsernameAndPassword(String username, String password);

    @Transactional
    void deleteByUsername(String username);
    
    //many to many GET
    List<Users> findAllUsersByAssociationsAssociationid(int associationid);
    List<Users> findAllUsersByGroupsGroupid(int groupid);
    List<Users> findAllUsersByLandLandid (int landid);
    List<Users> findAllUsersByEquipmentsEquipmentid (int equipmentid);

    //for search function
	List<Users> findByFirstnameOrLastname(String firstname,String lastname);
	List<Users> findByFirstnameAndLastname(String firstname,String lastname);
}
