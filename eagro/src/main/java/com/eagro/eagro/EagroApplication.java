package com.eagro.eagro;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EagroApplication {

	public static void main(String[] args) {
		SpringApplication.run(EagroApplication.class, args);
	}

}
