package com.eagro.eagro.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@Entity(name = "certificates")
public class Certificate {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int certificateid;
	
	private String name;
	private String category;
	private String duration;
	
	@ManyToMany(mappedBy = "certificates")
	@JsonIgnoreProperties("certificates")
	private List<Users> users;
	
	@ManyToMany(mappedBy = "certificates")
	@JsonIgnoreProperties("certificates")
	private List<Products> products;
	
	public int getCertificateid( ) {
		return certificateid;
	}
	public void setCertificateid(int certificateid) {
		this.certificateid = certificateid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getDuration() {
		return duration;
	}
	public void setDuration(String duration) {
		this.duration = duration;
	}
	public List<Users> getUsers() {
		return users;
	}
	public void setUsers(List<Users> users) {
		this.users = users;
	}
	public List<Products> getProducts() {
		return products;
	}
	public void setProducts(List<Products> products) {
		this.products = products;
	}
	
}