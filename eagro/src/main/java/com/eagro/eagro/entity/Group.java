package com.eagro.eagro.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity(name = "grups")
public class Group {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int groupid;

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "groupposts",
	joinColumns = @JoinColumn(name = "groupid", referencedColumnName = "groupid"),
	inverseJoinColumns = @JoinColumn(name = "postid", referencedColumnName = "postid"))
	@JsonIgnoreProperties("groups")
	private List<Post> posts;

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "groupmembers",
	joinColumns = @JoinColumn(name = "groupid", referencedColumnName = "groupid"),
	inverseJoinColumns = @JoinColumn(name = "username", referencedColumnName = "username"))
	@JsonIgnoreProperties("groups")
	private List<Users> users;

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "groupassociations", 
	joinColumns = @JoinColumn(name = "groupid", referencedColumnName = "groupid"),
	inverseJoinColumns = @JoinColumn(name = "associationid", referencedColumnName = "associationid"))
	@JsonIgnoreProperties("groups")
	private List<Associations> associations;

	private String name;
	
	private String owner;
	
	@OneToOne(mappedBy = "group")
	private Images image;

	public Images getImage() {
		return image;
	}

	public void setImage(Images image) {
		this.image = image;
	}

	public int getGroupid() {
		return groupid;
	}

	public void setGroupid(int groupid) {
		this.groupid = groupid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	private String category;

	public List<Post> getPosts() {
		return posts;
	}

	public void setPosts(List<Post> posts) {
		this.posts = posts;
	}

	public List<Users> getUsers() {
		return users;
	}

	public void setUsers(List<Users> users) {
		this.users = users;
	}

	public List<Associations> getAssociations() {
		return associations;
	}

	public void setAssociations(List<Associations> associations) {
		this.associations = associations;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

}
