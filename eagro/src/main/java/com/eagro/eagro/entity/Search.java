package com.eagro.eagro.entity;

import java.util.List;

public class Search {
	
	private List<Users> user;
	private List<Group> group;
	private List<Equipment> equipment;
	private List<Products> product;
	private List<Associations> association;
	
	

	public List<Products> getProduct() {
		return product;
	}


	public void setProduct(List<Products> product) {
		this.product = product;
	}


	public List<Associations> getAssociation() {
		return association;
	}


	public void setAssociation(List<Associations> association) {
		this.association = association;
	}


	public List<Users> getUser() {
		return user;
	}


	public void setUser(List<Users> user) {
		this.user = user;
	}


	public List<Group> getGroup() {
		return group;
	}


	public void setGroup(List<Group> group) {
		this.group = group;
	}


	public List<Equipment> getEquipment() {
		return equipment;
	}


	public void setEquipment(List<Equipment> equipment) {
		this.equipment = equipment;
	}
	
	

	
	

}
