package com.eagro.eagro.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity(name = "associations")
public class Associations {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int associationid;

	private String name;
	private float landarea;
	private String email;
	private Date foundingdate;
	private String website;
	private String headquarters;
	private String field;
	
	@OneToOne(mappedBy = "association")
	private Images image;

	public Images getImage() {
		return image;
	}

	public void setImage(Images image) {
		this.image = image;
	}

	@ManyToMany(mappedBy = "associations")
	@JsonIgnoreProperties("associations")
	private List<Group> groups;
	
	public List<Users> getUsers() {
		return users;
	}

	public void setUsers(List<Users> users) {
		this.users = users;
	}

	public List<Post> getPosts() {
		return posts;
	}

	public void setPosts(List<Post> posts) {
		this.posts = posts;
	}

	@ManyToMany(mappedBy = "associations")
	@JsonIgnoreProperties("associations")
    private List<Users> users;
	
    @ManyToMany(mappedBy = "associations")
    @JsonIgnoreProperties("associations")
    private List<Post> posts;
    
    
	public int getAssociationid() {
		return associationid;
	}

	public void setAssociationid(int associationid) {
		this.associationid = associationid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getLandarea() {
		return landarea;
	}

	public void setLandarea(float landarea) {
		this.landarea = landarea;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getFoundingdate() {
		return foundingdate;
	}

	public void setFoundingdate(Date foundingdate) {
		this.foundingdate = foundingdate;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getHeadquarters() {
		return headquarters;
	}

	public void setHeadquarters(String headquarters) {
		this.headquarters = headquarters;
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public List<Group> getGroups() {
		return groups;
	}

	public void setGroups(List<Group> groups) {
		this.groups = groups;
	}

}
