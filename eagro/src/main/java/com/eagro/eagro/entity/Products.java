package com.eagro.eagro.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity(name="products")
public class Products {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int productid;
	
	private String productname;
	private String producttype;
	private String fertilizer;
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "productcertificates", joinColumns = @JoinColumn(name = "productid", referencedColumnName = "productid"), inverseJoinColumns = @JoinColumn(name = "certificateid", referencedColumnName = "certificateid"))
	@JsonIgnoreProperties("products")
	private List<Certificate> certificates;
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "landproducts", joinColumns = @JoinColumn(name = "productid", referencedColumnName = "productid"), inverseJoinColumns = @JoinColumn(name = "landid", referencedColumnName = "landid"))
	@JsonIgnoreProperties("products")
	private List<Land> lands;
	
	@OneToOne(mappedBy = "product")
	private Images image;
	
	public Images getImage() {
		return image;
	}
	public void setImage(Images image) {
		this.image = image;
	}
	public List<Land> getLands() {
		return lands;
	}
	public void setLands(List<Land> lands) {
		this.lands = lands;
	}
	public List<Certificate> getCertificates() {
		return certificates;
	}
	public void setCertificates(List<Certificate> certificates) {
		this.certificates = certificates;
	}
	public int getProductid() {
		return productid;
	}
	public void setProductid(int productid) {
		this.productid = productid;
	}
	public String getProductname() {
		return productname;
	}
	public void setProductname(String productname) {
		this.productname = productname;
	}
	public String getProducttype() {
		return producttype;
	}
	public void setProducttype(String producttype) {
		this.producttype = producttype;
	}
	public String getFertilizer() {
		return fertilizer;
	}
	public void setFertilizer(String fertilizer) {
		this.fertilizer = fertilizer;
	}
	
	
}