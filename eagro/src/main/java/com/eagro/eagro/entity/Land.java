package com.eagro.eagro.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity(name = "lands")
public class Land {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int landid;

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "landowners", joinColumns = @JoinColumn(name = "landid", referencedColumnName = "landid"), inverseJoinColumns = @JoinColumn(name = "username", referencedColumnName = "username"))
	@JsonIgnoreProperties("lands")
	private List<Users> users;
	
	@ManyToMany(mappedBy = "lands")
	@JsonIgnoreProperties("lands")
	private List<Products> products;

	private String location;

	public int getLandid() {
		return landid;
	}

	public void setLandid(int landid) {
		this.landid = landid;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public float getArea() {
		return area;
	}

	public void setArea(float area) {
		this.area = area;
	}

	private float area;

	public List<Users> getUsers() {
		return users;
	}

	public void setUsers(List<Users> users) {
		this.users = users;
	}

}
