package com.eagro.eagro.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.sql.Date;
import java.util.List;

@Entity(name="equipments")
public class Equipment {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int equipmentid;
	
	@ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "ownedequipments", 
      joinColumns = @JoinColumn(name = "equipmentid", referencedColumnName = "equipmentid"), 
      inverseJoinColumns = @JoinColumn(name = "username", 
      referencedColumnName = "username"))
	@JsonIgnoreProperties("equipments")
    private List<Users> users;
	
	private String name;
	private String category;
	private Date productiondate;
	private int registrationnumber;
	private String brand;
	private String countryoforigin;
	
	public int getEquipmentid() {
		return equipmentid;
	}
	public void setEquipmentid(int equipmentid) {
		this.equipmentid = equipmentid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public Date getDate() {
		return productiondate;
	}
	public void setDate(Date productiondate) {
		this.productiondate = productiondate;
	}
	public int getRegistrationnumber() {
		return registrationnumber;
	}
	public void setRegistrationnumber(int registrationnumber) {
		this.registrationnumber = registrationnumber;
	}
	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}
	public String getCountryoforigin() {
		return countryoforigin;
	}
	public void setCountryoforigin(String countryoforigin) {
		this.countryoforigin = countryoforigin;
	}
	public List<Users> getUsers() {
		return users;
	}
	public void setUsers(List<Users> users) {
		this.users = users;
	}
	public Date getProductiondate() {
		return productiondate;
	}
	public void setProductiondate(Date productiondate) {
		this.productiondate = productiondate;
	}
	
}
