package com.eagro.eagro.entity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity(name="images")
public class Images {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int imageid;
	
	private String ppath;
	
	@OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "groupid", referencedColumnName = "groupid")
    private Group group;
	
	@OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "associationid", referencedColumnName = "associationid")
    private Associations association;
	
	@OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "productid", referencedColumnName = "productid")
    private Products product;
	
	private String profilepath;
	private String backgroundpath;
	
	@OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "username", referencedColumnName = "username")
	@JsonIgnoreProperties("image")
	private Users user;
	
	
	public Group getGroup() {
		return group;
	}
	public void setGroup(Group group) {
		this.group = group;
	}
	public Associations getAssociation() {
		return association;
	}
	public void setAssociation(Associations association) {
		this.association = association;
	}
	public Products getProduct() {
		return product;
	}
	public void setProduct(Products product) {
		this.product = product;
	}
	public String getProfilepath() {
		return profilepath;
	}
	public void setProfilepath(String profilepath) {
		this.profilepath = profilepath;
	}
	public String getBackgroundpath() {
		return backgroundpath;
	}
	public void setBackgroundpath(String backgroundpath) {
		this.backgroundpath = backgroundpath;
	}
	public int getImageid() {
		return imageid;
	}
	public void setImageid(int imageid) {
		this.imageid = imageid;
	}
	public String getPpath() {
		return ppath;
	}
	public void setPpath(String ppath) {
		this.ppath = ppath;
	}
	public Users getUser() {
		return user;
	}
	public void setUser(Users user) {
		this.user = user;
	}

}
