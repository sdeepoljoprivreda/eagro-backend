package com.eagro.eagro.entity;

import java.sql.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.JoinColumn;

@Entity(name = "users")
public class Users {

	@Id
	private String username;
	private String password;
	private String firstname;
	private String lastname;
	private String email;
	private String pib;
	private String phonenumber;
	private String website;
	private String address;
	private String profession;
	private String socialmedia;
	private int sendnews;
	private Date birthdate;
	
	@OneToOne(mappedBy = "user")
	@JsonIgnoreProperties("users")
	private Images image;
	
	public Images getImage() {
		return image;
	}

	public void setImage(Images image) {
		this.image = image;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinTable(name = "usersroles", joinColumns = @JoinColumn(name = "username"), inverseJoinColumns = @JoinColumn(name = "roleid"))
	private Set<Role> roles;

	@ManyToMany(mappedBy = "users")
	@JsonIgnoreProperties("users")
	private List<Equipment> equipments;

	@ManyToMany(mappedBy = "users")
	@JsonIgnoreProperties("users")
	private List<Group> groups;
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "ownedcertificates", joinColumns = @JoinColumn(name = "username", referencedColumnName = "username"), inverseJoinColumns = @JoinColumn(name = "certificateid", referencedColumnName = "certificateid"))
	@JsonIgnoreProperties("users")
	private List<Certificate> certificates;

	public List<Certificate> getCertificates() {
		return certificates;
	}

	public void setCertificates(List<Certificate> certificates) {
		this.certificates = certificates;
	}

	public List<Equipment> getEquipments() {
		return equipments;
	}

	public void setEquipments(List<Equipment> equipments) {
		this.equipments = equipments;
	}

	public List<Group> getGroups() {
		return groups;
	}

	public void setGroups(List<Group> groups) {
		this.groups = groups;
	}

	public List<Land> getLand() {
		return land;
	}

	public void setLand(List<Land> land) {
		this.land = land;
	}

	public List<Associations> getAssociations() {
		return associations;
	}

	public void setAssociations(List<Associations> associations) {
		this.associations = associations;
	}

	@ManyToMany(mappedBy = "users")
	@JsonIgnoreProperties("users")
	private List<Land> land;

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "associationmembers", joinColumns = @JoinColumn(name = "username", referencedColumnName = "username"), inverseJoinColumns = @JoinColumn(name = "associationid", referencedColumnName = "associationid"))
	@JsonIgnoreProperties("users")
	private List<Associations> associations;

	public Users(Users users) {
		this.username = users.getUsername();
		this.password = users.getPassword();
		this.firstname = users.getFirstname();
		this.lastname = users.getLastname();
		this.email = users.getEmail();
		this.pib = users.getPib();
		this.phonenumber = users.getPhonenumber();
		this.website = users.getWebsite();
		this.address = users.getAddress();
		this.profession = users.getProfession();
		this.socialmedia = users.getSocialmedia();
		this.sendnews = users.getSendnews();
		this.birthdate = users.getBirthdate();
		this.roles = users.getRoles();
		this.associations = users.getAssociations();
		this.land = users.getLand();
		this.groups = users.getGroups();
		this.equipments = users.getEquipments();
	}

	public Users() {
		super();
	}

	public String getUsername() {
		return username;
	}

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhonenumber() {
		return phonenumber;
	}

	public void setPhonenumber(String phonenumber) {
		this.phonenumber = phonenumber;
	}

	public String getPib() {
		return pib;
	}

	public void setPib(String pib) {
		this.pib = pib;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getSocialmedia() {
		return socialmedia;
	}

	public void setSocialmedia(String socialmedia) {
		this.socialmedia = socialmedia;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getProfession() {
		return profession;
	}

	public void setProfession(String profession) {
		this.profession = profession;
	}

	public int getSendnews() {
		return sendnews;
	}

	public void setSendnews(int sendnews) {
		this.sendnews = sendnews;
	}

	public Date getBirthdate() {
		return birthdate;
	}

	public void setBirthdate(Date birthdate) {
		this.birthdate = birthdate;
	}

}
