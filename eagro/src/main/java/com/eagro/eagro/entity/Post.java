package com.eagro.eagro.entity;

import java.util.List;
import java.sql.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity(name = "posts")
public class Post {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int postid;
	
	
	
	private String title;
	private String username;
	private Date date;
	private int parentid;

	@ManyToMany(mappedBy = "posts")
	@JsonIgnoreProperties("posts")
    private List<Group> groups;

	@ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "associationposts", 
    joinColumns = @JoinColumn (name = "postid", referencedColumnName = "postid") , 
    inverseJoinColumns = @JoinColumn (name = "associationid", referencedColumnName = "associationid"))
	@JsonIgnoreProperties("posts")
    private List<Associations> associations;
	
	public int getPostid() {
		return postid;
	}
	public void setPostid(int postid) {
		this.postid = postid;
	}
	
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	private String text;

	public List<Group> getGroups() {
		return groups;
	}
	public void setGroups(List<Group> groups) {
		this.groups = groups;
	}
	public List<Associations> getAssociations() {
		return associations;
	}
	public void setAssociations(List<Associations> associations) {
		this.associations = associations;
	}
	public int getParentid() {
		return parentid;
	}
	public void setParentid(int parentid) {
		this.parentid = parentid;
	}
	
	
	
	
}

