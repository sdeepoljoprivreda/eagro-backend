package com.eagro.eagro.entity;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.springframework.beans.factory.annotation.Autowired;

import com.eagro.eagro.repository.RefreshtokenRepository;
import com.eagro.eagro.repository.UserRepository;

@Entity(name = "refreshtoken")
public class Refreshtoken {
	
	@Id
	private String value;
	
	private String username;
	private Timestamp expirationdate;
	
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public Timestamp getExpirationdate() {
		return expirationdate;
	}
	public void setExpirationdate(Timestamp expirationdate) {
		this.expirationdate = expirationdate;
	}

}
