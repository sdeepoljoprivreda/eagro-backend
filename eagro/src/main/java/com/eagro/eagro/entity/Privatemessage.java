package com.eagro.eagro.entity;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name = "privatemessage")
public class Privatemessage {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int privatemessageid;
	
	private String sender;
	private int conversationid;
	private Date date;
	private String text;
	
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public int getPrivatemessageid() {
		return privatemessageid;
	}
	public void setPrivatemessageid(int privatemessageid) {
		this.privatemessageid = privatemessageid;
	}
	public String getSender() {
		return sender;
	}
	public void setSender(String sender) {
		this.sender = sender;
	}
	public int getConversationid() {
		return conversationid;
	}
	public void setConversationid(int conversationid) {
		this.conversationid = conversationid;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}

}
