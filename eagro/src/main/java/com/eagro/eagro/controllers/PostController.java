package com.eagro.eagro.controllers;

import java.util.List;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.eagro.eagro.entity.Post;
import com.eagro.eagro.repository.PostRepository;


@RestController
public class PostController {

	private final PostRepository repository;

	public PostController(PostRepository repository) {
		this.repository = repository;
	}

	@GetMapping("/posts")
	public List<Post> getAll() {
		return (List<Post>) repository.findAll();
	}

	@GetMapping("/posts/{postid}")
	public Post getPost(@PathVariable int postid) {
		return repository.findById(postid).orElse(null);
	}
	
	@GetMapping("/postsparent/{parentid}")
	public List<Post> getAllUnderParent(@PathVariable int parentid) {
		return (List<Post>) repository.findByParentid(parentid);
	}

	@GetMapping("/groupsposts/{groupid}")
	public List<Post> getPostByGroupId(@PathVariable int groupid){
		return (List<Post>) repository.findAllPostsByGroupsGroupid(groupid);
	}
	
	@GetMapping("/associationposts/{associationid}")
	public List<Post> findAllPostsByAssociationid(@PathVariable int associationid){
		return (List<Post>) repository.findAllPostsByAssociationsAssociationid(associationid);
	}

	@PostMapping("/posts")
	public Post createPost(@RequestBody Post post) {
		return repository.save(post);
	}	



	@DeleteMapping("/posts/{postid}")
	public boolean deletePost(@PathVariable int postid) {
		repository.deleteById(postid);
		return true;
	}

	@PutMapping("/posts/{postid}")
	public Post updatePost(@PathVariable int postid, @RequestBody Post post) {				
		return repository.save(post);
	}


}

