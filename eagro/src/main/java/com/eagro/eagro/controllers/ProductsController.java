package com.eagro.eagro.controllers;

import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.eagro.eagro.entity.Products;
import com.eagro.eagro.repository.ProductsRepository;


@RestController
public class ProductsController {

	private final ProductsRepository repository;

	public ProductsController(ProductsRepository repository) {
		this.repository = repository;
	}
	
	@CrossOrigin
	@GetMapping("/products")
	public List<Products> getAll() {
		return (List<Products>) repository.findAll();
	}

	@CrossOrigin
	@GetMapping("/products/{productsid}")
	public Products getProducts(@PathVariable int productsid) {
		return repository.findById(productsid).orElse(null);
	}


	@PostMapping("/products")
	public Products createProducts(@RequestBody Products products) {
		return repository.save(products);
	}	

	@DeleteMapping("/products/{productsid}")
	public boolean deleteProducts(@PathVariable int productsid) {
		repository.deleteById(productsid);
		return true;
	}

	@PutMapping("/products/{productsid}")
	public Products updateProducts(@PathVariable int productsid, @RequestBody Products products) {				
		return repository.save(products);
	}


}




