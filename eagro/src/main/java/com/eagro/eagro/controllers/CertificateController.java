package com.eagro.eagro.controllers;

import java.util.List;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.eagro.eagro.entity.Certificate;
import com.eagro.eagro.repository.CertificateRepository;
@RestController
public class CertificateController {
	private final CertificateRepository repository;
	
	public CertificateController(CertificateRepository repository) {
		this.repository = repository;
	}
	
	@CrossOrigin
	@GetMapping("/certificates")
	public List<Certificate> getAll() {
		return (List<Certificate>) repository.findAll();
	}
	@CrossOrigin
	@GetMapping("/certificates/{certificateid}")
	public Certificate getCertificate(@PathVariable int certificateid) {
		return repository.findById(certificateid).orElse(null);
	}
	@CrossOrigin
	@PostMapping("/certificates")
	public Certificate createCertificate(@RequestBody Certificate certificate) {
		return repository.save(certificate);
	}
	@CrossOrigin
	@DeleteMapping("/certificates/{certificateid}")
	public boolean deleleCertificate(@PathVariable int certificateid) {
		repository.deleteById(certificateid);
		return true;
	}
	@CrossOrigin
	@PutMapping("certificates/{certificateid}")
	public Certificate updateCertificate(@PathVariable int certificateid, @RequestBody Certificate certificate) {
		return repository.save(certificate);
	}
	
}
