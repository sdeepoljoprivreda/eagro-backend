package com.eagro.eagro.controllers;

import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


import com.eagro.eagro.entity.Conversation;

import com.eagro.eagro.repository.ConversationRepository;


@RestController
public class ConversationController {
	private final ConversationRepository repository;
		
	public ConversationController(ConversationRepository repository) {
		this.repository = repository;
	}
	
	@CrossOrigin
	@GetMapping("/conversations")
	public List<Conversation> getAll() {
		return (List<Conversation>) repository.findAll();
	}
	@CrossOrigin
	@GetMapping("/conversations/{conversationid}")
	public Conversation getConversation(@PathVariable int conversationid) {
		return repository.findById(conversationid).orElse(null);
	}
	@CrossOrigin
	@PostMapping("/conversations")
	public Conversation createConversation(@RequestBody Conversation conversation) {
		return repository.save(conversation);
	}
	@CrossOrigin
	@DeleteMapping("/conversation/{conversationid}")
	public boolean deleteConversation(@PathVariable int conversationid) {
		repository.deleteById(conversationid);
		return true;
	}
	@CrossOrigin
	@PutMapping("conversation/{conversationid}")
	public Conversation updateConversation(@PathVariable int conversationid, @RequestBody Conversation conversation) {
		return repository.save(conversation);
	}

}
