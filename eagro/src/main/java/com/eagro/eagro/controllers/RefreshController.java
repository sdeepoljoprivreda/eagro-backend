package com.eagro.eagro.controllers;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.eagro.eagro.config.SecurityConstants;
import com.eagro.eagro.entity.Products;
import com.eagro.eagro.entity.Refreshtoken;
import com.eagro.eagro.repository.PostRepository;
import com.eagro.eagro.repository.RefreshtokenRepository;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@RestController
public class RefreshController {
	
	@Autowired
	RefreshtokenRepository repository;

	
	
	@CrossOrigin
	@GetMapping("/refreshtoken")
	public String checkToken(@CookieValue(name = "refreshtoken") String refreshtoken,HttpServletResponse res,HttpServletRequest req) {
		
		Refreshtoken a=new Refreshtoken();
		a=repository.findById(refreshtoken).orElse(null);
		if(a != null) {
		String username = Jwts.parser()
                .setSigningKey( SecurityConstants.REFRESH_SECRET )
                .parseClaimsJws( refreshtoken )
                .getBody()
                .getSubject();
		
		
		
		String token=Jwts.builder()
				.setSubject(username)
				.setExpiration(new Date(System.currentTimeMillis() + SecurityConstants.EXPIRATION_TIME))
                .signWith(SignatureAlgorithm.HS512, SecurityConstants.TOKEN_SECRET )
                .compact();
		res.addHeader(SecurityConstants.HEADER_STRING, SecurityConstants.TOKEN_PREFIX +  token);
		res.addHeader("username", username);
		return username;
		}
		res.setStatus(401);
		return null;
	}
	@CrossOrigin
	@DeleteMapping("/refreshtoken")
	public String deleteToken(@CookieValue(name = "refreshtoken") String refreshtoken,HttpServletResponse res,HttpServletRequest req) {
		
		Refreshtoken a=new Refreshtoken();
		a=repository.findById(refreshtoken).orElse(null);
		if(a != null) {
		repository.deleteById(refreshtoken);
		res.setStatus(200);
		return null;
		}
		res.setStatus(404);
		return null;
	}

}
