package com.eagro.eagro.controllers;

import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.eagro.eagro.entity.Land;
import com.eagro.eagro.repository.LandRepository;


@RestController
public class LandController {

	private final LandRepository repository;

	public LandController(LandRepository repository) {
		this.repository = repository;
	}
	@GetMapping("/landowned/{username}")
	public List<Land> getLandByusername(@PathVariable String username){
		return (List<Land>) repository.findAllLandByUsersUsername(username);
	}

	@GetMapping("/lands")
	public List<Land> getAll() {
		return (List) repository.findAll();
	}

	@GetMapping("/lands/{landid}")
	public Land getLand(@PathVariable int landid) {
		return repository.findById(landid).orElse(null);
	}


	@PostMapping("/lands")
	public Land createLand(@RequestBody Land land) {
		return repository.save(land);
	}	

	@DeleteMapping("/lands/{landid}")
	public boolean deleteLand(@PathVariable int landid) {
		repository.deleteById(landid);
		return true;
	}

	@PutMapping("/lands/{landid}")
	public Land updateLand(@PathVariable int landid, @RequestBody Land land) {				
		return repository.save(land);
	}


}




