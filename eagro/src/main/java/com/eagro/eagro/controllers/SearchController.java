package com.eagro.eagro.controllers;



import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


import com.eagro.eagro.entity.Search;
import com.eagro.eagro.entity.SearchRequestModel;
import com.eagro.eagro.entity.Users;
import com.eagro.eagro.repository.AssociationsRepository;
import com.eagro.eagro.repository.EquipmentRepository;
import com.eagro.eagro.repository.GroupRepository;
import com.eagro.eagro.repository.ProductsRepository;
import com.eagro.eagro.repository.UserRepository;

@RestController
public class SearchController {
	
	private final UserRepository userRepository;
	private final EquipmentRepository equipmentRepository;
	private final GroupRepository groupRepository;
	private final ProductsRepository productRepository;
	private final AssociationsRepository associationRepository;
	
	public SearchController(UserRepository userRepository,EquipmentRepository equipmentRepository,GroupRepository groupRepository,AssociationsRepository associationRepository,ProductsRepository productRepository) {
	    this.associationRepository = associationRepository;
		this.productRepository = productRepository;
		this.equipmentRepository = equipmentRepository;
		this.groupRepository = groupRepository;
		this.userRepository = userRepository;
	}
	
	@CrossOrigin
	@PostMapping("/search")
	public Search getSearchResults(@RequestBody SearchRequestModel s){
		Search search=new Search();
		List<Users> users1=new ArrayList();
		List<Users> users2=new ArrayList();
		List<Users> users=new ArrayList();
		
		if(s.getKeywords().contains(" ") && (s.getFilters().isEmpty()||s.getFilters().contains("all")||s.getFilters().contains("users")) ) {
			String[] splitted = s.getKeywords().split(" ");
			users1=(userRepository.findByFirstnameAndLastname(splitted[0],splitted[1]));
		}
		
		if(s.getFilters().contains("all")||s.getFilters().isEmpty())
		{
		search.setProduct(productRepository.findByProductname(s.getKeywords()));
		search.setAssociation(associationRepository.findByName(s.getKeywords()));
		users2=(userRepository.findByFirstnameOrLastname(s.getKeywords(),s.getKeywords()));
		search.setEquipment(equipmentRepository.findByName(s.getKeywords()));
		search.setGroup(groupRepository.findByName(s.getKeywords()));
		}else {
		if(s.getFilters().contains("products"))
		{
		search.setProduct(productRepository.findByProductname(s.getKeywords()));
		}
		if(s.getFilters().contains("associations"))
		{
		search.setAssociation(associationRepository.findByName(s.getKeywords()));
		}
		if(s.getFilters().contains("users"))
		{
		users2=(userRepository.findByFirstnameOrLastname(s.getKeywords(),s.getKeywords()));
		}
		if(s.getFilters().contains("equipment"))
		{
		search.setEquipment(equipmentRepository.findByName(s.getKeywords()));
		}
		if(s.getFilters().contains("groups"))
		{
		search.setGroup(groupRepository.findByName(s.getKeywords()));
		}
		
		}
		users.addAll(users1);
		users.addAll(users2);
		
		search.setUser(users);
		
		return search;
	}

}
