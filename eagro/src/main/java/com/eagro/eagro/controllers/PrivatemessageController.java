package com.eagro.eagro.controllers;

import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.eagro.eagro.entity.Conversation;
import com.eagro.eagro.entity.Privatemessage;
import com.eagro.eagro.repository.ConversationRepository;
import com.eagro.eagro.repository.PrivatemessageRepository;

@RestController
public class PrivatemessageController {
	private final PrivatemessageRepository repository;
		
	public PrivatemessageController(PrivatemessageRepository repository) {
		this.repository = repository;
	}
	
	@CrossOrigin
	@GetMapping("/privatemessages")
	public List<Privatemessage> getAll() {
		return (List<Privatemessage>) repository.findAll();
	}
	@CrossOrigin
	@GetMapping("/privatemessages/{privatemessagesid}")
	public Privatemessage getPrivatemessage(@PathVariable int privatemessageid) {
		return repository.findById(privatemessageid).orElse(null);
	}
	@CrossOrigin
	@PostMapping("/privatemessages")
	public Privatemessage createPrivatemessage(@RequestBody Privatemessage privatemessage) {
		return repository.save(privatemessage);
	}
	@CrossOrigin
	@DeleteMapping("/privatemessages/{privatemessagesid}")
	public boolean deletePrivatemessages(@PathVariable int privatemessageid) {
		repository.deleteById(privatemessageid);
		return true;
	}
	@CrossOrigin
	@PutMapping("privatemessages/{privatemessageid}")
	public Privatemessage updatePrivatemessage(@PathVariable int privatemessageid, @RequestBody Privatemessage privatemessage) {
		return repository.save(privatemessage);
	}

}
