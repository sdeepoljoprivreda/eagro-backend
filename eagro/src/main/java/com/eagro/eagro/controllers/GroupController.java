package com.eagro.eagro.controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.eagro.eagro.config.SecurityConstants;
import com.eagro.eagro.entity.Group;
import com.eagro.eagro.repository.GroupRepository;

import io.jsonwebtoken.Jwts;

@RestController
public class GroupController {

	private final GroupRepository repository;

	public GroupController(GroupRepository repository) {
		this.repository = repository;
	}

	@GetMapping("/groups")
	public List<Group> getAll() {
		return (List<Group>) repository.findAll();
	}

	@GetMapping("/groups/{groupid}")
	public Group getGroup(@PathVariable int groupid) {
		return repository.findById(groupid).orElse(null);
	}
	
	

	@PostMapping("/groups")
	public Group createGroup(@RequestBody Group group,HttpServletRequest req) {
		String token = req.getHeader(SecurityConstants.HEADER_STRING);
		token = token.replace(SecurityConstants.TOKEN_PREFIX, "");
		String username = Jwts.parser()
                .setSigningKey( SecurityConstants.TOKEN_SECRET )
                .parseClaimsJws( token )
                .getBody()
                .getSubject();
		group.setOwner(username);
		return repository.save(group);
	}	

	@DeleteMapping("/groups/{groupid}")
	public boolean deleteGroup(@PathVariable int groupid) {
		repository.deleteById(groupid);
		return true;
	}

	@PutMapping("/groups/{groupid}")
	public Group updateGroup(@PathVariable int groupid, @RequestBody Group group) {				
		return repository.save(group);
	}


}




