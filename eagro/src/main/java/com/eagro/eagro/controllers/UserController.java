package com.eagro.eagro.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.eagro.eagro.entity.Group;
import com.eagro.eagro.entity.Post;
import com.eagro.eagro.entity.Users;
import com.eagro.eagro.repository.UserRepository;

@RestController
public class UserController {

	private final UserRepository repository;
	
	
	BCryptPasswordEncoder bCryptPasswordEncoder=new BCryptPasswordEncoder();

	public UserController(UserRepository repository) {
		this.repository = repository;
	}

	@CrossOrigin
	@GetMapping("/users")
	public List<Users> getAll() {
		return (List) repository.findAll();
	}
	

	@CrossOrigin
	@GetMapping("/users/{id}")
	public Users getUser(@PathVariable String id) {
		return repository.findById(id).orElse(null);
	}
	
	
	

	@CrossOrigin
	@PostMapping("/users")
	public Users createUser(@RequestBody Users user) {
		user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
		return repository.save(user);
	}	

	@CrossOrigin
	@DeleteMapping("/users/{id}")
	public boolean deleteUser(@PathVariable String id) {
		repository.deleteById(id);
		return true;
	}

	@CrossOrigin
	@PutMapping("/users/{id}")
	public Users updateUser(@PathVariable String id, @RequestBody Users user) {		
		return repository.save(user);
	}
	

}