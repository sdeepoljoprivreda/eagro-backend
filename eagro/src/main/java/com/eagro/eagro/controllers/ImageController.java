package com.eagro.eagro.controllers;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.eagro.eagro.config.SecurityConstants;
import com.eagro.eagro.entity.Images;
import com.eagro.eagro.entity.Users;
import com.eagro.eagro.repository.AssociationsRepository;
import com.eagro.eagro.repository.GroupRepository;
import com.eagro.eagro.repository.ImageRepository;
import com.eagro.eagro.repository.ProductsRepository;
import com.eagro.eagro.repository.UserRepository;

import io.jsonwebtoken.Jwts;

@RestController
public class ImageController {
	
	private final ImageRepository repository;
	
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private GroupRepository groupRepository;
	@Autowired
	private AssociationsRepository associationRepository;
	@Autowired
	private ProductsRepository productRepository;

	public ImageController(ImageRepository repository) {
		this.repository = repository;
	}
	
	
	
	
	@CrossOrigin
	@PostMapping("/images/users/profile")
	public Images createUserProfileImage(@RequestParam("image") MultipartFile imageFile,HttpServletRequest req) throws Exception {
		String folder="C:/Users/Andrej/Desktop/EagroPictures/Users/";
		Images image=new Images();
		
		String token = req.getHeader(SecurityConstants.HEADER_STRING);
		token = token.replace(SecurityConstants.TOKEN_PREFIX, "");
		String username = Jwts.parser()
                .setSigningKey( SecurityConstants.TOKEN_SECRET )
                .parseClaimsJws( token )
                .getBody()
                .getSubject();
		Users user=userRepository.findByUsername(username).orElse(null);
		
		if (user == null || user.getImage() == null) {
			new File(folder.concat(username).concat("/").concat("Profile")).mkdirs();
		}
		else {
		image=user.getImage();
		if (!Files.exists(Paths.get(folder.concat(username)))) {
		    new File(folder.concat(username)).mkdir();
		}
		
		if (Files.exists(Paths.get(folder.concat(username).concat("/").concat("Profile")))) {
		    new File(image.getProfilepath()).delete();
		}
		
		}

		new File(folder.concat(username).concat("/").concat("Profile")).mkdir();
		
		byte[] bytes=imageFile.getBytes();
		
		Path path=Paths.get(folder.concat(username).concat("/").concat("Profile").concat("/").concat(imageFile.getOriginalFilename()));
		Files.write(path, bytes);
		if(image.getUser()==null) {
			image.setUser(user);
		}
		image.setProfilepath(folder.concat(username).concat("/").concat("Profile").concat("/").concat(imageFile.getOriginalFilename()));
		
		
		return repository.save(image);
	}
	
	@CrossOrigin
	@PostMapping("/images/users/background")
	public Images createUserBackgroundImage(@RequestParam("image") MultipartFile imageFile,HttpServletRequest req) throws Exception {
		String folder="C:/Users/Andrej/Desktop/EagroPictures/Users/";
		Images image=new Images();
		
		String token = req.getHeader(SecurityConstants.HEADER_STRING);
		token = token.replace(SecurityConstants.TOKEN_PREFIX, "");
		String username = Jwts.parser()
                .setSigningKey( SecurityConstants.TOKEN_SECRET )
                .parseClaimsJws( token )
                .getBody()
                .getSubject();
		Users user=userRepository.findByUsername(username).orElse(null);
		
		if (user == null || user.getImage() == null) {
			new File(folder.concat(username).concat("/").concat("Background")).mkdirs();
		}
		else {
		image=user.getImage();
		if (!Files.exists(Paths.get(folder.concat(username)))) {
		    new File(folder.concat(username)).mkdir();
		}
		
		if (Files.exists(Paths.get(folder.concat(username).concat("/").concat("Background")))) {
		    new File(image.getBackgroundpath()).delete();
		}
		
		}

		new File(folder.concat(username).concat("/").concat("Background")).mkdir();
		
		byte[] bytes=imageFile.getBytes();
		
		Path path=Paths.get(folder.concat(username).concat("/").concat("Background").concat("/").concat(imageFile.getOriginalFilename()));
		Files.write(path, bytes);
		if(image.getUser()==null) {
		image.setUser(user);
		}
		image.setBackgroundpath(folder.concat(username).concat("/").concat("Background").concat("/").concat(imageFile.getOriginalFilename()));
		
		
		return repository.save(image);
	}
	
	/*
	
	@CrossOrigin
	@PostMapping("/images/users/background")
	public Images createUserBackgroundImage(@RequestParam("image") MultipartFile imageFile,HttpServletRequest req) throws Exception {
		String folder="C:/Users/Andrej/Desktop/EagroPictures/Users/";
		Images image=new Images();
		
		String token = req.getHeader(SecurityConstants.HEADER_STRING);
		token = token.replace(SecurityConstants.TOKEN_PREFIX, "");
		String username = Jwts.parser()
                .setSigningKey( SecurityConstants.TOKEN_SECRET )
                .parseClaimsJws( token )
                .getBody()
                .getSubject();
		
		if (repository.findByUsername(username) == null) {
			new File(folder.concat(username).concat("/").concat("Background")).mkdirs();
		}
		else {
		image=repository.findByUsername(username);
		if (!Files.exists(Paths.get(folder.concat(username)))) {
		    new File(folder.concat(username)).mkdir();
		}
		
		if (Files.exists(Paths.get(folder.concat(username).concat("/").concat("Background")))) {
		    new File(image.getBackgroundpath()).delete();
		}
		
		}

		new File(folder.concat(username).concat("/").concat("Background")).mkdir();
		
		byte[] bytes=imageFile.getBytes();
		
		Path path=Paths.get(folder.concat(username).concat("/").concat("Background").concat("/").concat(imageFile.getOriginalFilename()));
		Files.write(path, bytes);
		//image.setUsername(username);
		image.setBackgroundpath(folder.concat(username).concat("/").concat("Background").concat("/").concat(imageFile.getOriginalFilename()));
		//image.setAssociationid(0);
		//image.setGroupid(0);
		//image.setProductid(0);
		image.setPpath("");
		if(image.getProfilepath()==null) {
			image.setProfilepath("");
			}
		
		
		return repository.save(image);
	}
	
	@CrossOrigin
	@PostMapping("/images/groups/profile")
	public Images createGroupProfileImage(@RequestParam("image") MultipartFile imageFile,@RequestParam("groupid") int groupid,HttpServletRequest req) throws Exception {
		
		String folder="C:/Users/Andrej/Desktop/EagroPictures/Groups/";
		Images image=new Images();
		
		if (repository.findByGroupid(groupid) == null) {
			new File(folder.concat(String.valueOf(groupid)).concat("/").concat("Profile")).mkdirs();
		}
		else {
		image=repository.findByGroupid(groupid);
		if (!Files.exists(Paths.get(folder.concat(String.valueOf(groupid))))) {
		    new File(folder.concat(String.valueOf(groupid))).mkdir();
		}
		
		if (Files.exists(Paths.get(folder.concat(String.valueOf(groupid)).concat("/").concat("Profile")))) {
		    new File(image.getProfilepath()).delete();
		}
		
		}

		new File(folder.concat(String.valueOf(groupid)).concat("/").concat("Profile")).mkdir();
		
		byte[] bytes=imageFile.getBytes();
		
		Path path=Paths.get(folder.concat(String.valueOf(groupid)).concat("/").concat("Profile").concat("/").concat(imageFile.getOriginalFilename()));
		Files.write(path, bytes);
		//image.setUsername("");
		image.setProfilepath(folder.concat(String.valueOf(groupid)).concat("/").concat("Profile").concat("/").concat(imageFile.getOriginalFilename()));
		//image.setAssociationid(0);
		//image.setGroupid(groupid);
		//image.setProductid(0);
		image.setPpath("");
		if(image.getBackgroundpath()==null) {
			image.setBackgroundpath("");
			}
		
		
		return repository.save(image);
	}
	
	@CrossOrigin
	@PostMapping("/images/groups/background")
	public Images createGroupBackgroundImage(@RequestParam("image") MultipartFile imageFile,@RequestParam("groupid") int groupid,HttpServletRequest req) throws Exception {
		
		String folder="C:/Users/Andrej/Desktop/EagroPictures/Groups/";
		Images image=new Images();
		
		if (repository.findByGroupid(groupid) == null) {
			new File(folder.concat(String.valueOf(groupid)).concat("/").concat("Background")).mkdirs();
		}
		else {
		image=repository.findByGroupid(groupid);
		if (!Files.exists(Paths.get(folder.concat(String.valueOf(groupid))))) {
		    new File(folder.concat(String.valueOf(groupid))).mkdir();
		}
		
		if (Files.exists(Paths.get(folder.concat(String.valueOf(groupid)).concat("/").concat("Background")))) {
		    new File(image.getProfilepath()).delete();
		}
		
		}

		new File(folder.concat(String.valueOf(groupid)).concat("/").concat("Background")).mkdir();
		
		byte[] bytes=imageFile.getBytes();
		
		Path path=Paths.get(folder.concat(String.valueOf(groupid)).concat("/").concat("Background").concat("/").concat(imageFile.getOriginalFilename()));
		Files.write(path, bytes);
		image.setBackgroundpath(folder.concat(String.valueOf(groupid)).concat("/").concat("Background").concat("/").concat(imageFile.getOriginalFilename()));
		//image.setAssociationid(0);
		//image.setGroupid(groupid);
		//image.setProductid(0);
		image.setPpath("");
		if(image.getProfilepath()==null) {
			image.setProfilepath("");
			}
		
		
		return repository.save(image);
	}
	*/

}
