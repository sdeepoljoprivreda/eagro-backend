package com.eagro.eagro.controllers;

import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.eagro.eagro.entity.Associations;
import com.eagro.eagro.repository.AssociationsRepository;


@RestController
public class AssociationsController {

	private final AssociationsRepository repository;

	public AssociationsController(AssociationsRepository repository) {
		this.repository = repository;
	}
	
	@CrossOrigin
	@GetMapping("/associations")
	public List<Associations> getAll() {
		return (List<Associations>) repository.findAll();
	}

	@CrossOrigin
	@GetMapping("/associations/{associationid}")
	public Associations getAssociation(@PathVariable int associationid) {
		return repository.findById(associationid).orElse(null);
	}
	
	@GetMapping("/groupassociations/{groupid}")
	public List<Associations> getPostByGroupId(@PathVariable int groupid){
		return (List<Associations>) repository.findAllAssociationsByGroupsGroupid(groupid);
	}

	@GetMapping("/usersassociation/{username}")
	public List<Associations> getAssociationByUsername(@PathVariable String username){
		return (List<Associations>) repository.findAllAssociationsByUsersUsername(username);
	}

	@PostMapping("/associations")
	public Associations createAssociation(@RequestBody Associations associations) {
		return repository.save(associations);
	}	

	@DeleteMapping("/associations/{associationid}")
	public boolean deleteAssociation(@PathVariable int associationid) {
		repository.deleteById(associationid);
		return true;
	}

	@PutMapping("/associations/{associationid}")
	public Associations updateAssociations(@PathVariable int associationid, @RequestBody Associations associations) {				
		return repository.save(associations);
	}


}




