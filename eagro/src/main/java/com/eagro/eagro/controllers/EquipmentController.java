package com.eagro.eagro.controllers;

import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.eagro.eagro.entity.Equipment;
import com.eagro.eagro.entity.Group;
import com.eagro.eagro.repository.EquipmentRepository;
@RestController
public class EquipmentController {
	
	private final EquipmentRepository repository;
	
	public EquipmentController(EquipmentRepository repository) {
		this.repository = repository;
	}
	
	@CrossOrigin
	@GetMapping("/equipments")
	public List<Equipment> getAll() {
		return (List) repository.findAll();
	}
	
	@GetMapping("/equipmentused/{username}")
	public List<Equipment> getEquipmentByUsername(@PathVariable String username){
		return (List<Equipment>) repository.findAllEquipmentByUsersUsername(username);
	}
	
	@CrossOrigin
	@GetMapping("/equipments/{equipmentid}")
	public Equipment getEquipment(@PathVariable int equipmentid) {
		return repository.findById(equipmentid).orElse(null);
	}
	@CrossOrigin
	@PostMapping("/equipments")
	public Equipment createEquipment(@RequestBody Equipment equipment) {
		return repository.save(equipment);
	}	
	@CrossOrigin
	@DeleteMapping("/equipments/{equipmentid}")
	public boolean deleteEquipment(@PathVariable int equipmentid) {
		repository.deleteById(equipmentid);
		return true;
	}
	@CrossOrigin
	@PutMapping("/equipments/{equipmentid}")
	public Equipment updateEquipment(@PathVariable int equipmentid, @RequestBody Equipment equipment) {				
		return repository.save(equipment);
	}
}
